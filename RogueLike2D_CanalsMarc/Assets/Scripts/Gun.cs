﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Gun : MonoBehaviour
{
    public WeaponSC actualWeapon;
    private GameObject bullet;
    public GameObject BulletPrefab, ArrowPrefab;
    private SpriteRenderer playerSR;
    private Sprite gunSprite;
    private Transform shotpoint;
    private Rigidbody2D bulletRB;
    private float speed = 3f;
    [SerializeField]
    private int damage = 0, bulletAmount, actualGun, magazineSize;
    private float fireRate = 0, reloadTime = 0, nBales = 1, reserveBulletAmount = 0;
    private string name;
    private bool isShooting = false, hasReload = false;
    public List<WeaponSC> weapons;
    // Start is called before the first frame update
    void Start()
    {
        playerSR = this.gameObject.GetComponent<SpriteRenderer>();
        shotpoint = this.gameObject.GetComponent<Transform>();
        actualWeapon = this.gameObject.GetComponent<WeaponController>().weaponInfo;
        initGun();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.X))
        {

            if (actualGun == 4)
            {
                actualGun = 0;
            }
            else
            {
                actualGun++;
            }
            UpdateGunInfo();
        }
        Shoot();
    }

    private void Shoot()
    {
        if (bulletAmount > 0)
        {
            if (Input.GetButtonDown("Fire1"))
            {
                if (!isShooting)
                {
                    isShooting = true;
                    if (nBales > 0)
                    {
                        bulletAmount--;
                        for (int i = 1; i <= nBales; i++)
                        {
                            bullet = Instantiate(BulletPrefab, shotpoint.position + new Vector3(0.25f, 0), shotpoint.rotation);
                            GameManager.Instance.actualWeapon.bulletAmount = bulletAmount;
                            //We get the RB of the bullet to add the force
                            bulletRB = bullet.GetComponent<Rigidbody2D>();
                            //Add force to position
                            switch (i)
                            {
                                case 1:
                                    bulletRB.AddRelativeForce(new Vector2(1, 0) * speed, ForceMode2D.Impulse);
                                    break;
                                case 2:
                                    bulletRB.AddRelativeForce(new Vector2(1, -0.5f) * speed, ForceMode2D.Impulse);
                                    break;
                                case 3:
                                    bulletRB.AddRelativeForce(new Vector2(1, 0.5f) * speed, ForceMode2D.Impulse);
                                    break;
                                case 4:
                                    bulletRB.AddRelativeForce(new Vector2(1, 0.25f) * speed, ForceMode2D.Impulse);
                                    break;
                                case 5:
                                    bulletRB.AddRelativeForce(new Vector2(1, -0.25f) * speed, ForceMode2D.Impulse);
                                    break;
                                case 6:
                                    bulletRB.AddRelativeForce(new Vector2(1, 0.75f) * speed, ForceMode2D.Impulse);
                                    break;
                                case 7:
                                    bulletRB.AddRelativeForce(new Vector2(1, -0.75f) * speed, ForceMode2D.Impulse);
                                    break;
                                case 8:
                                    bulletRB.AddRelativeForce(new Vector2(1, -1) * speed, ForceMode2D.Impulse);
                                    break;
                                case 9:
                                    bulletRB.AddRelativeForce(new Vector2(1, 1) * speed, ForceMode2D.Impulse);
                                    break;
                                default:
                                    break;
                            }
                        }
                    }
                    else
                    {
                        bullet = Instantiate(ArrowPrefab, shotpoint.position + new Vector3(0.25f, 0), shotpoint.rotation);
                        bulletAmount--;
                        GameManager.Instance.actualWeapon.bulletAmount = bulletAmount;
                        bulletRB = bullet.GetComponent<Rigidbody2D>();
                        if (playerSR.flipY == false)
                        {
                            bullet.GetComponent<Transform>().rotation = Quaternion.Euler(0, 0, 270);
                        }
                        else
                        {
                            bullet.GetComponent<Transform>().rotation = Quaternion.Euler(0, 0, 90);
                        }
                        bulletRB.AddRelativeForce(new Vector2(1, 0) * speed, ForceMode2D.Impulse);
                    }
                }
                else
                {
                    StartCoroutine(waitForFireRate());
                }
            }
        }
        else
        {
            if (reserveBulletAmount > 0)
            {
                if (!hasReload)
                {
                    StartCoroutine(waitForReload());
                }
            }
            else
            {
                reserveBulletAmount = 0;
                Debug.Log("Not enough ammmo for this gun, try changing");
            }
        }
    }
    IEnumerator waitForReload()
    {
        hasReload = true;
        yield return new WaitForSeconds(reloadTime * GameManager.Instance.maxReloadTimeMultiplier);
        //Update BulletAmount
        bulletAmount = actualWeapon.MagazineSize;

        //Update bullet reserve
        reserveBulletAmount -= actualWeapon.MagazineSize;

        //Update currentWeaponSC
        actualWeapon.bulletAmount = bulletAmount;
        actualWeapon.ReserveBulletAmount = reserveBulletAmount;

        //Update Manager
        GameManager.Instance.actualWeapon.bulletAmount = bulletAmount;
        GameManager.Instance.actualWeapon.ReserveBulletAmount = reserveBulletAmount;
        hasReload = false;

    }
    IEnumerator waitForFireRate()
    {
        yield return new WaitForSeconds(fireRate);
        isShooting = false;
    }
    public void UpdateGunInfo()
    {
        //Cargar data weapon
        actualWeapon = weapons[actualGun];
        GameManager.Instance.actualWeapon = actualWeapon;
        nBales = actualWeapon.BulletsPerShot;
        gunSprite = actualWeapon.sprite;
        playerSR.sprite = gunSprite;
        damage = actualWeapon.Damage;
        if (actualWeapon.ReserveBulletAmount >= actualWeapon.MagazineSize) {
            bulletAmount = actualWeapon.MagazineSize;
        }
        else 
        {
            bulletAmount = 0;
        }
        actualWeapon.bulletAmount = bulletAmount;
        reserveBulletAmount = actualWeapon.ReserveBulletAmount * GameManager.Instance.maxCapacityMultiplier;
        fireRate = actualWeapon.FireRateTime;
        reloadTime = actualWeapon.ReloadTime;
        name = actualWeapon.name;
        this.gameObject.GetComponent<WeaponController>().weaponInfo = actualWeapon;
        Debug.Log("Changing guns to: " + name);
    }
        //1st gun init
        private void initGun()
        {
            actualWeapon = weapons[0];
            GameManager.Instance.actualWeapon = actualWeapon;
            gunSprite = actualWeapon.sprite;
            playerSR.sprite = gunSprite;
            damage = actualWeapon.Damage;
            bulletAmount = actualWeapon.MagazineSize;
            actualWeapon.bulletAmount = bulletAmount;
            actualWeapon.ReserveBulletAmount = 32;
            reserveBulletAmount = actualWeapon.ReserveBulletAmount;
            fireRate = actualWeapon.FireRateTime;
            reloadTime = actualWeapon.ReloadTime;
            name = actualWeapon.name;
            actualGun = 0;
        }
    }
