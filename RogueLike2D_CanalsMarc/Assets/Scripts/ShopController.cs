﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class ShopController : MonoBehaviour
{
    public List<GameObject> shopItems;
    public List<PassiveItem> passiveItems;
    public TextMeshPro titleText, priceText, buffText, debuffText;
    // Start is called before the first frame update
    void Start()
    {
        initShop();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void initShop() 
    { 
        for(int i = 0; i < shopItems.Capacity; i++) 
        {
            putItemsInHolders(i);
        }
    }

    void putItemsInHolders(int index) 
    {
        int randomPassive = Random.Range(0, passiveItems.Capacity);

        shopItems[index].GetComponent<SpriteRenderer>().sprite = passiveItems[randomPassive].getImage();
        titleText = shopItems[index].transform.GetChild(0).GetComponent<TextMeshPro>();
        priceText = shopItems[index].transform.GetChild(1).GetComponent<TextMeshPro>();
        buffText = shopItems[index].transform.GetChild(2).GetComponent<TextMeshPro>();
        debuffText = shopItems[index].transform.GetChild(3).GetComponent<TextMeshPro>();

        shopItems[index].GetComponent<ShopData>().passiveItem = passiveItems[randomPassive];

        titleText.text = passiveItems[randomPassive].getName();
        priceText.text = passiveItems[randomPassive].getPrice().ToString();
        buffText.text = (passiveItems[randomPassive].effects[0].effectValue)+" "+passiveItems[randomPassive].effects[0].effect;
        debuffText.text = (passiveItems[randomPassive].effects[1].effectValue) + " " + passiveItems[randomPassive].effects[1].effect;
    }
}
