﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    private Vector3 velocity = Vector3.zero;
    private Transform playerTF;
    // Start is called before the first frame update
    void Start()
    {
        playerTF = GameManager.Instance.player.transform;
    }

    // Update is called once per frame
    void Update()
    {
        if (playerTF.position.y < transform.position.y - 2f)
        {
            transform.position = Vector3.SmoothDamp(transform.position, new Vector3(transform.position.x, transform.position.y - 3f, transform.position.z), ref velocity, 0.25f);
        }
        if (playerTF.position.y > transform.position.y + 2f) {

            transform.position = Vector3.SmoothDamp(transform.position, new Vector3(transform.position.x, transform.position.y + 3f, transform.position.z), ref velocity, 0.25f);
        }
        if (playerTF.position.x > transform.position.x + 3f)
        {
            transform.position = Vector3.SmoothDamp(transform.position, new Vector3(transform.position.x + 3f, transform.position.y, transform.position.z), ref velocity, 0.25f);
        }
        if (playerTF.position.x < transform.position.x - 3f)
        {
            transform.position = Vector3.SmoothDamp(transform.position, new Vector3(transform.position.x - 3f, transform.position.y, transform.position.z), ref velocity, 0.25f);
        }
    }
}


