﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TurretEnemyController : MonoBehaviour
{
    public GameObject bulletPrefab, bullet;
    public float sWaitTime, shotWaitTime, speed = 10f;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        shoot();
    }


    private void shoot()
    {
        if (sWaitTime <= 0)
        {
            Vector2 aim = GameManager.Instance.player.GetComponent<Rigidbody2D>().position - this.GetComponent<Rigidbody2D>().position;
            bullet = Instantiate(bulletPrefab, transform.position, Quaternion.Euler(0f, 0f, Mathf.Atan2(aim.y, aim.x) * Mathf.Rad2Deg));
            bullet.GetComponent<Rigidbody2D>().AddRelativeForce(new Vector2(1, 0) * speed, ForceMode2D.Force);
            sWaitTime = shotWaitTime;

        }
        sWaitTime -= Time.deltaTime;
    }
}
