﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class maxScoreControll : MonoBehaviour
{
    public Text maxScoreText;
    // Start is called before the first frame update
    void Start()
    {
        maxScoreText.text = "Max Score: "+PlayerPrefs.GetInt("MaxScore");
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
