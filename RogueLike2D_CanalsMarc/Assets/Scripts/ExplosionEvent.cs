﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExplosionEvent : MonoBehaviour
{
    public delegate void Contact();
    public event Contact OnContactExplosion;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (OnContactExplosion != null)
        {
            if(collision.CompareTag("MinionTrigger"))
            OnContactExplosion();
        }
    }
}
