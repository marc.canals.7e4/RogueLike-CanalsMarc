﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DropItemController : MonoBehaviour
{
    [SerializeField]
    private ItemListDropper itemsList;
    private List<ItemDataSC> itemList;
    // Start is called before the first frame update
    void Start()
    {
        Invoke("getRandom", 0.4f);
    }
    // Update is called once per frame
    void Update()
    {
    
    }
    private void OnDestroy()
    {
        
    }
    void getRandom()
    {
            GameObject item = itemsList.getRndItem();
            item.transform.position = gameObject.transform.position;
            Destroy(this.gameObject);
    }
}
