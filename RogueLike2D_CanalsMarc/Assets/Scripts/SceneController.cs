﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneController : MonoBehaviour
{
    public void MenuLoader()
    {
        SceneManager.LoadScene(0, LoadSceneMode.Single);
    }
    public void SettingsLoader()
    {
        SceneManager.LoadScene(1, LoadSceneMode.Single);
    }
    public void GameLoader()
    {
        SceneManager.LoadScene(2, LoadSceneMode.Single);
    }
    public void Exit()
    {
        Application.Quit();
    }
}
