﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    private Animator playerAnim;
    private SpriteRenderer sprite, spriteGun;
    private Rigidbody2D playerRB;
    private GameObject placeholderItem;
    private float horizontal, vertical, speed = 10f, moveLimiter = 0.7f, dashCD = 2, dashSpeed = 3;
    public bool canIDash = true, isDashing = false;
    private Vector2 saveVel;
    // Start is called before the first frame update
    void Start()
    {

        playerAnim = gameObject.GetComponent<Animator>();
        sprite = gameObject.GetComponent<SpriteRenderer>();
        playerRB = gameObject.GetComponent<Rigidbody2D>();
        spriteGun = GameObject.Find("ShotPoint").GetComponent<SpriteRenderer>();
    }

    // Update is called once per frame
    void Update()
    {
        horizontal = Input.GetAxis("Horizontal");
        vertical = Input.GetAxis("Vertical");
        FollowCursor();
        Dash();

    }
    private void FixedUpdate()
    {
        if (isDashing == false)
        {
            playerAnim.SetBool("isDashing", false);
            if (horizontal != 0 && vertical != 0)
            {
                horizontal *= moveLimiter;
                vertical *= moveLimiter;
            }
            if (horizontal != 0 || vertical != 0)
            {
                playerAnim.SetBool("isWalking", true);
            }
            else
            {
                playerAnim.SetBool("isWalking", false);
                playerRB.velocity = Vector2.zero;
            }
            playerRB.velocity = new Vector2(horizontal * speed * GameManager.Instance.speedMultiplier, vertical * speed * GameManager.Instance.speedMultiplier);
        }
    }

    private void FollowCursor()
    {
        //Get the mouse Point
        Vector2 mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        //Get the vector between mousePos and Player position
        Vector2 aim = mousePos - playerRB.position;
        //Angle between each other
        float angle = Mathf.Atan2(aim.y, aim.x) * Mathf.Rad2Deg;
        RotatePlayer(angle);
    }
    private void RotatePlayer(float angle)
    {
        //Rotates the player into the point where player shot!
        if (angle > 90 || angle < -90)
        {
            spriteGun.flipY = true;
            sprite.flipY = true;
        }
        else
        {
            spriteGun.flipY = false;
            sprite.flipY = false;
        }
        gameObject.GetComponent<Transform>().rotation = Quaternion.Euler(0f, 0f, angle);


    }
    private void Dash()
    {
        if (Input.GetKeyDown(KeyCode.Space) && canIDash == true)
        {
            isDashing = true;
            saveVel = playerRB.velocity;
            playerAnim.SetBool("isDashing", true);
            playerRB.velocity = new Vector2(saveVel.x * dashSpeed, saveVel.y * dashSpeed);
            StartCoroutine(DashCD());

        }
    }
    IEnumerator DashCD()
    {
        canIDash = false;
        yield return new WaitForSeconds(0.15f);
        isDashing = false;
        playerAnim.SetBool("isDashing", false);
        playerRB.velocity = Vector2.zero;
        yield return new WaitForSeconds(dashCD);
        canIDash = true;

    }
    IEnumerator disablePlaceholder()
    {
        yield return new WaitForSeconds(1f);
        placeholderItem.SetActive(false);

    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Item"))
        {
            collision.GetComponent<ItemController>().itemData.ReserveBulletAmount += 10;
            Destroy(collision.gameObject);
        }
        if (collision.CompareTag("Coin")) 
        {
            Destroy(collision.gameObject);
            GameManager.Instance._coins += 1;
        }
        if (collision.CompareTag("ShopItem")) 
        {
            placeholderItem = collision.gameObject;
            PassiveItem item = collision.gameObject.GetComponent<ShopData>().passiveItem;
            if (!collision.GetComponent<ShopData>().alreadyBought)
            {
                collision.GetComponent<ShopData>().alreadyBought = true;
                if (GameManager.Instance.canBuy(item.getPrice()))
                {
                    GameManager.Instance.buyItem(item);
                    GameManager.Instance._coins -= item.getPrice();
                    StartCoroutine(disablePlaceholder());
                }
            }
        }

        if (collision.CompareTag("Door"))
        {
            if (GameManager.Instance.gridFinished){
                GameManager.Instance.changeGridMap();
            }
        }
    }
}
  
