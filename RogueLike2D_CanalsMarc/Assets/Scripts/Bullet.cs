﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{
    private DropItemController drop;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        //Player bullet
        if (this.gameObject.CompareTag("Bullet"))
        {
            if (collision.CompareTag("Border"))
            {
                Destroy(this.gameObject);
            }

            if (collision.CompareTag("Destructable"))
            {
                drop = collision.gameObject.GetComponent<DropItemController>();
                drop.enabled = true;
                Destroy(this.gameObject);
            }

            if (collision.CompareTag("Enemy"))
            {
                GameManager.Instance.dmgEnemy(collision.gameObject, GameManager.Instance.actualWeapon.Damage);
                Destroy(this.gameObject);
            }
        }
        else 
        {
            //Turret bullet
            if (collision.CompareTag("Player")) 
            {
                GameManager.Instance.getDmg(10);
                Destroy(this.gameObject);
            }
            if (collision.CompareTag("Border"))
            {
                Destroy(this.gameObject);
            }

        }
    }
}
