﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnerEnemies : MonoBehaviour
{
    public List<Transform> spawnPoints;
    public int maxEnemics = 10, numSpawner;
    public GameObject enemy;
    // Start is called before the first frame update
    void Start()
    {
        GameManager.Instance.enemiesInRoom = maxEnemics;
        InvokeRepeating("CreateEnemies", 0.4f, 2);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void CreateEnemies()
    {
        if (GameManager.Instance.playerHealth > 0)
        {
            if (maxEnemics != 0)
            {
                numSpawner = Random.Range(0, spawnPoints.Count);
                maxEnemics--;
                Instantiate(enemy, new Vector3(spawnPoints[numSpawner].position.x, spawnPoints[numSpawner].position.y, 0), Quaternion.identity);
            }
        }
    }
}
