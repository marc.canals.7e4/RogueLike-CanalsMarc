﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameOverScript : MonoBehaviour
{
    public Text pointsText;
    public int points;
    public void Setup()
    {
        gameObject.SetActive(true);
        points = GameManager.Instance.getScore();
        pointsText.text = "POINTS: " + points;
        
        
    }
    public void MenuButton()
    {
        Application.Quit();
    }
    public void RestartButton()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }
}
