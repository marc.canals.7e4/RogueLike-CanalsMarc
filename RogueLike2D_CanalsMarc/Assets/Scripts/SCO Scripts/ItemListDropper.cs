﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "ItemListDropper", menuName = "ScriptableObjects/ItemListDropper", order = 1)]
public class ItemListDropper : ScriptableObject
{
    public List<WeaponSC> items;

    public GameObject ItemDroppablePrefab;

    public GameObject getRndItem()
    {
        GameObject item = Instantiate(ItemDroppablePrefab);
        WeaponSC itemData = items[Random.Range(0, items.Count)];
        item.GetComponent<ItemController>().itemData = itemData;
        item.GetComponent<SpriteRenderer>().sprite = itemData.iconSprite;
        return item;
    }
}
