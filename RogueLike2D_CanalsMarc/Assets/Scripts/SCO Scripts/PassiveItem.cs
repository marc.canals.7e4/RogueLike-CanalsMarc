﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "PassiveItem", menuName = "ScriptableObjects/Items", order = 1)]
public class PassiveItem : Item
{
    [System.Serializable]
    public struct Pasive {
        public string effect;
        public float effectValue;
    }

    public Pasive[] effects;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
