﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "DroppableData", menuName = "ScriptableObjects/DroppableData")]

public class DroppablesSC : ItemDataSC
{
}
