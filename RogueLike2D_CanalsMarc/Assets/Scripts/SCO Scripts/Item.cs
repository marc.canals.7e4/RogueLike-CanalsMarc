﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Item : ScriptableObject
{
    [SerializeField]
    private string name;
    [SerializeField]
    private float chance;
    [SerializeField]
    private float price;
    [SerializeField]
    private string itemType;
    [SerializeField]
    private Sprite image;


    public string getName()
    {
        return this.name;
    }
    public string getItemType()
    {
        return this.itemType;
    }

    public float getPrice()
    {
        return this.price;
    }
    public float getChance()
    {
        return this.chance;
    }
    public Sprite getImage()
    {
        return this.image;
    }
}
