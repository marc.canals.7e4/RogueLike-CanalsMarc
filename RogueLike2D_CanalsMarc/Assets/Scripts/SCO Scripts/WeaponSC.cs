﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "WeaponData", menuName = "ScriptableObjects/WeaponData")]
public class WeaponSC : ItemDataSC
{
    public Sprite sprite;
    public int Damage;
    public int MagazineSize;
    public int bulletAmount;
    public float ReserveBulletAmount;

    public string name;
    public float FireRateTime;
    public float ReloadTime;
    public float BulletsPerShot;
}
