﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemDataSC : ScriptableObject
{
    public Sprite iconSprite;
    public int amount;
}
