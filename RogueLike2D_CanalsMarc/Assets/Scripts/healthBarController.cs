﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class healthBarController : MonoBehaviour
{
    private Image HealthBar;
    public float currentHealth;
    private float maxHealth;
    // Start is called before the first frame update
    void Start()
    {
        HealthBar = GetComponent<Image>();
        maxHealth = 100;
    }

    // Update is called once per frame
    void Update()
    {
        maxHealth = GameManager.Instance.maxPlayerHealth;
        currentHealth = GameManager.Instance.playerHealth;
        HealthBar.fillAmount = currentHealth / maxHealth;
    }
}
