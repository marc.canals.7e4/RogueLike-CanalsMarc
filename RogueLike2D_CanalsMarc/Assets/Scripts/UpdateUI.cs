﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UpdateUI : MonoBehaviour
{
    public Text pointsText;
    public Text ammoText;
    public Image actualGunSprite;
    public Image nextGunSprite;
    public Text coinsText;
    public Text HpText;
    private float maxBulletCap, bulletsInMagazine;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        HpText.text = GameManager.Instance.playerHealth +" / "+GameManager.Instance.maxPlayerHealth;
        coinsText.text = GameManager.Instance._coins.ToString();
        maxBulletCap = GameManager.Instance.actualWeapon.ReserveBulletAmount;
        bulletsInMagazine = GameManager.Instance.actualWeapon.bulletAmount;
        actualGunSprite.sprite = GameManager.Instance.actualWeapon.sprite;
       // nextGunSprite.sprite = GameManager.Instance.weapons[];
        ammoText.text = bulletsInMagazine + " / " + maxBulletCap;
        pointsText.text = GameManager.Instance._score + " POINTS";
    }
}
