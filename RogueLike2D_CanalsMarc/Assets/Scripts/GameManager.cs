﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    public int _score = 0;
    public float _coins = 0;
    public int _ammo = 0;
    public int _Maxammo = 0;
    public int enemiesKilledInRoom;
    public int enemiesInRoom;
    public float playerHealth;
    public float maxPlayerHealth;
    public float maxHealthMultiplier = 1;
    public float dmgMultiplier = 1;
    public float speedMultiplier = 1;
    public float maxReloadTimeMultiplier = 1;
    public float maxCapacityMultiplier = 1;
    public bool gridFinished = false;
    public WeaponSC actualWeapon;
    [SerializeField]
    GameObject[] rooms;
    private int currentRoomId = 0;
    public List<WeaponSC> weapons;
    public GameObject[] enemies, items;
    public int actualGunIndex;
    public GameOverScript gameOver;
    public int maxScore;
    public GameObject player, finishLevelText, Coin;
    private static GameManager _instance;
    public static GameManager Instance
    {
        get
        {
            return _instance;
        }
    }

    private void Awake()
    {
        if (_instance != null && _instance != this)
        {
            Destroy(this.gameObject);
        }
        _instance = this;
        //DontDestroyOnLoad(this);
    }

    // Start is called before the first frame update
    void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player");
        _score = 0;
        maxPlayerHealth = 100;
    }
    public void AddScore()
    {
        _score += 5;
    }
    public int getScore()
    {
        return _score;
    }
    public void getDmg(int dmg)
    {
        playerHealth -= dmg;
    }
    public void dmgEnemy(GameObject enemy, float gunDmg)
    {
        enemy.GetComponent<EnemyConstants>().hp -= gunDmg * dmgMultiplier;
        enemy.GetComponentInChildren<ParticleSystem>().Play();
        if (enemy.GetComponent<EnemyConstants>().hp <= 0)
        {
            AddScore();
            Instantiate(Coin, enemy.GetComponent<Transform>().transform.position, Quaternion.identity);
            Destroy(enemy.gameObject);
            enemiesKilledInRoom++;
        }

    }
    public bool canBuy(float price) 
    {
        if (_coins >= price)
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    void NextRoom()
    {
        gridFinished = false;
        _score += 250;
        GameObject salaAnterior = GameObject.FindGameObjectWithTag("Level");
        if (salaAnterior != null)
        {
                Destroy(salaAnterior);     
        }
        currentRoomId = Random.Range(0, rooms.Length);
        enemiesKilledInRoom = 0;
        Instantiate(rooms[currentRoomId], new Vector3(0, 0, 0), Quaternion.identity);
        Debug.Log("Changing room to : "+currentRoomId);
        finishLevelText.SetActive(false);
        if (currentRoomId == 3) {
            StartCoroutine(waitForFinishingGrid());
            enemiesInRoom = 0;
            finishLevelText.SetActive(false);

        }
    }
    public IEnumerator waitForFinishingGrid() 
    {
        yield return new WaitForSeconds(2f);
        gridFinished = true;
    }
    public void buyItem(PassiveItem item) 
    {
        for (int i = 0; i < 2; i++) 
        {
            switch (item.effects[i].effect)
            {
                case "DMG":
                    dmgMultiplier += item.effects[i].effectValue;
                    break;
                case "HP":
                    if (playerHealth + item.effects[i].effectValue > maxPlayerHealth) {
                        playerHealth = maxPlayerHealth;
                    }
                    else
                    {
                        playerHealth += item.effects[i].effectValue;
                    }
                    break;
                case "MAXHP":
                    maxHealthMultiplier += item.effects[i].effectValue;
                    if(maxHealthMultiplier == 0)
                    {
                        maxHealthMultiplier = 0.25f;
                    }
                    maxPlayerHealth *= maxHealthMultiplier;
                    break;
                case "AMMO":
                    actualWeapon.ReserveBulletAmount += item.effects[i].effectValue;
                    break;
                case "SPEED":
                    speedMultiplier += item.effects[i].effectValue;
                    break;
                case "RPM":
                    maxReloadTimeMultiplier += item.effects[i].effectValue*(-1);
                    break;
                case "CAPACITY":
                    maxCapacityMultiplier += item.effects[i].effectValue;
                    break;
                case "BULLET":
                    actualWeapon.BulletsPerShot += item.effects[i].effectValue;
                    break;
                default:
                    Debug.Log("Info not found for: "+item.effects[i].effect);
                    break;
            }
        }
        
        player.GetComponentInChildren<Gun>().UpdateGunInfo();
    }
    public void enemiesLeft() 
    {
        if (GameManager.Instance.enemiesKilledInRoom >= GameManager.Instance.enemiesInRoom && currentRoomId != 3)
        {
            GameManager.Instance.finishLevelText.SetActive(true);
            GameManager.Instance.gridFinished = true;
        }
    }
    public void changeGridMap()
    {
        deleteAllEnemies();
        deleteAllItems();
        NextRoom();
        player.transform.position = new Vector2(0, 0);
    }
    // Update is called once per frame
    void Update()
    {
        if (playerHealth <= 0)
        {
            gameOver.Setup();
            player.SetActive(false);
            scoreUpload();
            deleteAllEnemies();
            deleteAllItems();
        }
        enemiesLeft();
        
    }
    public void scoreUpload() 
    {
        maxScore = PlayerPrefs.GetInt("MaxScore");
        if (_score > maxScore) {
            PlayerPrefs.SetInt("MaxScore", _score);
        }
    }
    public void deleteAllEnemies()
    {
        enemies = GameObject.FindGameObjectsWithTag("Enemy");
        for (int i = 0; i < enemies.Length; i++)
        {
            Destroy(enemies[i]);
        }
    }
    public void deleteAllItems()
    {
        items = GameObject.FindGameObjectsWithTag("Item");
        for (int i = 0; i < items.Length; i++)
        {
            Destroy(items[i]);
        }
    }
}
