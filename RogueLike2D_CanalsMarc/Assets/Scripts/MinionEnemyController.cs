﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MinionEnemyController : MonoBehaviour
{
    private bool isNearPlayer = false;
    public float speed = 3;
    private Transform playerPos;
    private GameObject player;
    private Transform minionTF;
    private Animator minionAnim;
    public ExplosionEvent explosion;
    private GameObject explosionRange;
    // Start is called before the first frame update
    void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player");
        minionAnim = gameObject.GetComponent<Animator>();
        minionTF = gameObject.GetComponent<Transform>();
        explosion.OnContactExplosion += dmgPlayer;
    }

    // Update is called once per frame
    void Update()
    {

    }
    private void OnDisable()
    {
        explosion.OnContactExplosion -= dmgPlayer;
    }
    private void FixedUpdate()
    {
        float step = speed * Time.deltaTime;
        playerPos = player.GetComponent<Transform>();
        minionTF = gameObject.GetComponent<Transform>();
        if (!isNearPlayer)
        {
            minionTF.position = Vector3.MoveTowards(minionTF.position, playerPos.position, step);
        }
    }
    private void dmgPlayer()
    {
        isNearPlayer = true;
        minionAnim.SetBool("isExploding", true);
        if (!GameManager.Instance.player.GetComponent<PlayerController>().isDashing)
        {
            GameManager.Instance.getDmg(15);
            Debug.Log("Got dmg: " + GameManager.Instance.playerHealth);
        }
        else { Debug.Log("*Esquivo esquivo*"); }
    }

    private void kill() 
    {
        Destroy(this.gameObject);
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("MinionTrigger")) 
        {
            isNearPlayer = true;
        }
        
    }
}
